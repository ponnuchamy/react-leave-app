import React from 'react';
import './Home.scss';

const Home = () =>  (
    <div className='d-flex justify-content-center align-items-center p-4'>
        <h1>HOME PAGE</h1>
    </div>
);

export default Home;