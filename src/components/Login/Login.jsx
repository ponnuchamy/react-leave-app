import React from 'react';
import './Login.scss';
import LoginForm from './LoginForm';

const Login = () => (
  <div id="login-form" className="container">
    <div className="row">
        <div className="col-md-12 min-vh-100 d-flex flex-column justify-content-center">
            <div className="row">
                <div className="col-lg-6 col-md-8 mx-auto">

                    <div className="card rounded shadow shadow-sm">
                        <div className="card-header">
                            <h3 className="mb-0">Login</h3>
                        </div>
                        <div className="card-body">
                            <LoginForm/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  </div>
);

export default Login;
