import React, { useState } from 'react';
import TextFieldGroup from '../common/TextFieldGroup';
import validateInput from '../../validations/login';
const LoginForm = () => {
  const [loginCredential, setLoginCredential] = useState({
    userName: '',
    password: '',
    errors: {},
    isLoading: false
  });

  const handleLoginSubmit = e => {
    e.preventDefault();
    console.log('Submit: ', loginCredential);
    if (isValid()) {
        setLoginCredential({...loginCredential, errors: {}, isLoading: true });
        // this.props.login(this.state).then(
        //   (res) => this.context.router.push('/'),
        //   (err) => this.setState({ errors: err.response.data.errors, isLoading: false })
        // );
      }
  };

  const isValid = () => {
    const { errors, isValid } = validateInput(loginCredential);

    if (!isValid) {
        setLoginCredential({...loginCredential, errors });
    }

    return isValid;
  };

  const onChange = e =>
    setLoginCredential({
      ...loginCredential,
      [e.target.name]: e.target.value
    });

  return (
    <form
      className="form"
      autoComplete="off"
      id="formLogin"
      noValidate
      onSubmit={handleLoginSubmit}
    >
    { loginCredential.errors.form && <div className="alert alert-danger">{loginCredential.errors.form}</div> }
      <TextFieldGroup
        field="userName"
        label="Username"
        value={loginCredential.userName}
        error={loginCredential.errors.userName}
        onChange={onChange}
      />

      <TextFieldGroup
        field="password"
        label="Password"
        value={loginCredential.password}
        error={loginCredential.errors.password}
        onChange={onChange}
        type="password"
      />

      <div>
        <label className="custom-control custom-checkbox">
          <input type="checkbox" className="custom-control-input" />
          <span className="custom-control-indicator"></span>
          <span className="custom-control-description small text-dark">
            Remember me on this computer
          </span>
        </label>
      </div>
      <button
        type="submit"
        className="btn btn-success btn-lg float-right"
        id="btnLogin"
        disabled={loginCredential.isLoading}
      >
        Login
      </button>
      {JSON.stringify(loginCredential)}
    </form>
  );
};

export default LoginForm;
