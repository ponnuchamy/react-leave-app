import { LOGIN } from '../constants/types';

export const createLoginRequest = user => ({
    type: LOGIN.REQUEST_START,
    user
});


