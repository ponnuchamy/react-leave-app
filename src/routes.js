import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './components/Home/Home';
import Login from './components/Login/Login';
import PageNotFound from './components/PageNotFound/PageNotFound';
import { Provider } from 'react-redux';
import configureStore from './services/store/configureStore';

const store = configureStore();

const routes = () => (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/404" exact component={PageNotFound}/>
            </Switch>
        </Router>
    </Provider>
);

export default routes;