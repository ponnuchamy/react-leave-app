import axios from 'axios';
import { clearSessionAndRedirect } from '../storage/session';
const request = axios.create({
    baseURL: 'https://safe-self-23642.herokuapp.com/'
});

request.interceptors.request.use(data => {
    data.headers.common = { ...data.headers.common, Authorization: 'Bearer ' };
    return data;
});

request.interceptors.response.use(response => {
    if (response.status === 401) {
        clearSessionAndRedirect();
    } else {
        return response;
    }
    return 0;
});