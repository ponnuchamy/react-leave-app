import { composeWithDevTools } from 'redux-devtools-extension';
import createSageMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../../reducers/rootReducer';
import rootSaga from '../../saga/rootSaga';

// mount it on the store
const configureStore = () => {
    //create the saga middleware
    const sagaMiddleware = createSageMiddleware();
    const store = createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;