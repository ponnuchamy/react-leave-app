
const setToken = data => sessionStorage.setItem('token' , data);

const getToken = () => sessionStorage.getItem('token');

const clearSession = () => sessionStorage.clear();

const clearSessionAndRedirect = () => {
    sessionStorage.clear();
    window.location.href = 'http://localhost:3000';
};

export default {setToken, getToken, clearSession, clearSessionAndRedirect};