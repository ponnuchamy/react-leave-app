import { LOGIN } from '../constants/types';

const intitalState = {
    login: null,
    isLoaded: false,
    error: null
};

const loginReducer = (state = intitalState, action = null) => {
    switch (action.type) {
        case LOGIN.REQUEST_START:
            return {
                ...state,
                isLoaded: false
            };
        case LOGIN.REQUEST_FULFILLED:
            return {
                ...state,
                isLoaded: true,
                login: action.payload
            };
        case LOGIN.REQUEST_REJECTED:
            return {
                ...state,
                isLoaded: false,
                error: action.payload
            };
    
        default:
            return state;
    }
};

export default loginReducer;