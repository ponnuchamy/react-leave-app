import Validator from 'validator';
import { isEmpty } from '../helpers/valueHelpers';

export default function validateInput(data) {
  let errors = {};

  if (isEmpty(data.userName)) {
    errors.userName = 'This field is required';
  }

  if (isEmpty(data.password)) {
    errors.password = 'This field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};