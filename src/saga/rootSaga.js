import { LOGIN } from '../constants/types';
import { createLoginSage } from './loginSaga';
import { takeLatest } from 'redux-saga/effects';

export function* rootSaga() {
    yield takeLatest(LOGIN.REQUEST_START, createLoginSage);
}


export default rootSaga;