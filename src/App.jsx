import React from 'react';
import './App.scss';
import Routes from './routes';


const App = () => (
  <div className="container-fluid">
    <Routes/>
  </div>
);

export default App;
